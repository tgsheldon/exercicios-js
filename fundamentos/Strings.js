const escola = "Cod3r"

console.log(escola.charAt(4))
console.log(escola.charAt(5))
console.log(escola.charCodeAt(3))
console.log(escola.indexOf('o'))
console.log(escola.substring(2))
console.log(escola.substring(0, 4))
console.log("escola ".concat(escola).concat('!'))
console.log("escola " + escola + '!')
console.log(escola.replace(3, 'e'))
console.log('Ana,Maria,Pedro'.split(','))