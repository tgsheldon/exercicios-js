console.log('a =', a)
var a = 2 // hoisting é o efeito de içar 
console.log('a =', a) // não é indicado fazer desse jeito

console.log('a =', a)
let a = 2 // o efeito de içamento não funciona com let
console.log('a =', a)